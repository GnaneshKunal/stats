# Elements of structured Data

## Data Types

### Continuous

Data that can take on any value in an interval
<b>Synonyms:</b>
Interval, float, numeric

### Discrete
Data	that	can	take	on	only	integer	values,	such	as	counts.
<b>Synonyms:</b>
integer,	count

### Categorical
Data	that	can	take	on	only	a	specific	set	of	values	representing	a	set	of	possible	categories.
<b>Synonyms:</b>
enums,	enumerated,	factors,	nominal,	polychotomous

### Binary
A	special	case	of	categorical	data	with	just	two	categories	of	values	(0/1,	true/false).
<b>Synonyms:</b>
dichotomous,	logical,	indicator,	boolean

### Ordinal
Categorical	data	that	has	an	explicit	ordering.

## Rectangular Data

### Data	frame
Rectangular	data	(like	a	spreadsheet)	is	the	basic	data	structure	for	statistical	and	machine
learning	models.

### Feature
A	column	in	the	table	is	commonly	referred	to	as	a	feature.
<b>Synonyms:</b>
attribute,	input,	predictor,	variable

### Outcome
Many	data	science	projects	involve	predicting	an	outcome	—	often	a	yes/no	outcome	(in	Table	1-
1,	it	is	“auction	was	competitive	or	not”).	The	features	are	sometimes	used	to	predict	the	outcome
in	an	experiment	or	study.
<b>Synonyms</b>
dependent	variable,	response,	target,	output

### Records
A row in the table is commonly referred to as a record.
<b>Synonyms</b>
case,	example,	instance,	observation,	pattern,	sample