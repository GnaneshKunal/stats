#!/usr/bin/env python3

from chap1 import *
import os
import csv
import numpy as np
import pandas as pd
from matplotlib import  pyplot

print(mean_from_csv('../data/state.csv', r=1, header=True))

print(median_from_csv('../data/state.csv', header=True))



print(sd_from_csv('../data/state.csv', r=1, header=True))
print(iqr_r_from_csv('../data/state.csv', r=1, header=True))
print(mad_from_csv('../data/state.csv', r=1, header=True))

print(np.std(dat_from_csv('../data/state.csv', r=1, header=True)))
q75, q25 = np.percentile(dat_from_csv('../data/state.csv', r=1, header=True), [75, 25])

print(q75 - q25)

print(iqr_reduced([1,2,3,3,5,6,7,9]))
print(percentile_reduced([1,2,3,3,5,6,7,9], 0.75))

# q5, q25, q50, q75, q95
murder_rate = np.percentile(dat_from_csv(
    '../data/state.csv', r=2, header=True
), [.50])

state = pd.read_csv('../data/state.csv')

print(state.quantile([.05, .25, .5, .75, .95]))

pyplot.boxplot(state["Population"])
pyplot.show()

def myrange(start, step, count):
    return range(start, start + (step * count), step)

breaks = (myrange(min(state["Population"]), max(state['Population']), 10))

pyplot.hist(state["Population"])
pyplot.show()

flight_delays = dat_row_from_csv('../data/dfw_airline.csv', header=None)

# pyplot.bar(flight_delays)

# T 1.000 0.475 0.328 0.678 0.279
# CTL 0.475 1.000 0.420 0.417 0.287
# FTR 0.328 0.420 1.000 0.287 0.260
# VZ 0.678 0.417 0.287 1.000 0.242
# LVLT 0.279 0.287 0.260 0.242 1.000

T = [1.0, 0.475, 0.328, 0.678, 0.279]
VZ = [0.678, 0.417, .287, 1.0, .242]

pyplot.scatter(T, VZ)
pyplot.show()

