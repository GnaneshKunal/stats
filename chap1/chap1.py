#!/usr/bin/env python3

import csv
import os
from math import ceil
from collections import Counter
import random

x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
y = [100, 20, 56, 23]


mrng_values = [62, 67, 71, 74, 76, 77, 78, 79, 79, 80, 80, 81, 81, 82, 83, 84, 86, 89, 93, 98]

mrng_weights = [81, 82, 83, 84, 85, 86, 87, 87, 88, 88, 89, 89, 89, 90, 90, 90, 90, 91, 91, 91, 92, 92, 93, 93, 94, 95, 96, 97, 98, 99]

#
#

def mean(x):
    """
        Mean
        The	sum	of	all	values	divided	by	the	number	of	values.
        Synonyms average
    """
    return sum(x) / len(x)

# print(mean(x))

def weighted_mean(x, y):
    """
        Weighted mean
        The	sum	of	all	values	times	a	weight	divided	by	the	sum	of	the	weights.
        Synonyms weighted	average
    """
    return sum(x1 * y1 for x1, y1 in zip(x, y)) / sum(y)

# print(weighted_mean(mrng_values, mrng_weights))

def median(x):
    """
    Median
    The	value	such	that	one-half	of	the	data	lies	above	and	below.
    Synonyms
    50th percentile
    """
    n = len(x)
    if n < 1:
        return None
    if n % 2 == 1:
        return sorted(x)[n // 2]
    else:
        return sum(sorted(x)[n // 2-1:n // 2 + 1])/2.0

def trimmed_mean(x, t=0.0):
    assert isinstance(t, float), "Please pass an float"
    assert t <= 1, "Trim length exceed list length"
    total_trim = int(len(x) * round(t, 1))
    if total_trim <= 0 or total_trim > len(x):
        return mean(x)
    elif total_trim > 0:
        lst = sorted(x)[total_trim:-total_trim]
        return sum(lst) / len(lst)

# print(trimmed_mean(x, 1))

def mean_from_csv(fil, r=1, weighted=False, w=2, header=False, trim=0.0):
    assert os.path.basename(fil).endswith('csv'), 'Please pass a csv file'
    assert trim <= 1, "Please pass a valid trim argument"
    
    with open(fil, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        if header:
            next(reader, None) # skip headers
        data = []
        if weighted:
            weight = []
        for row in reader:
            data.append(float(row[r]))
            if weighted:
                weight.append(float(row[w]))
        if weighted:
            return weighted_mean(data, weight)
        else:
            return trimmed_mean(data, trim)

def median_from_csv(fil, r=1, header=False):
    assert os.path.basename(fil).endswith('csv'), 'Please pass a csv file'
    with open(fil, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        if header:
            next(reader, None) # skip headers
        data = []
        for row in reader:
            data.append(int(row[r]))
        return median(data)

def mean_absolute_deviation(x):
    _mean = mean(x)
    sum = 0
    for x1 in x:
        sum += abs(x1 - _mean)
    return sum / len(x)

def variance(x):
    _mean = mean(x)
    sum = 0
    for x1 in x:
        sum += (x1 - _mean) ** 2
    return sum / len(x) - 1

def standard_deviation(x):
    return variance(x) ** (1 / 2)

def median_absolute_deviation(x):
    _mean = mean(x)
    data = []
    for x1 in x:
        data.append(abs(x1 - _mean))
    return median(data)

def percentile(x, p):
    assert 0.0 <= p <= 1.0, "Pass a valid percent"
    _x = sorted(x)
    n = max(int(round(p * len(_x) + 0.5)), 2)
    return _x[n - 2]

def percentile_reduced(x, p):
    assert 0.0 <= p <= 1.0, "Pass a valid percent"
    _x = sorted(x)
    if p < 0.5:
        return median(_x[:ceil((len(_x) / p) / 10)])
    return median(_x[ceil((len(_x) / p) / 10) + 2:])

def per(x, p):
    assert 0.0 <= p <= 1.0, "Pass a valid percent"
    _x = sorted(x)
    flag = False
    

def iqr(x):
    return abs(percentile(x, 0.25) - percentile(x, 0.75))

def iqr_reduced(x):
    # print(percentile_reduced(x, 0.25), percentile_reduced(x, 0.75))
    return abs(percentile_reduced(x, 0.25) - percentile_reduced(x, 0.75))


def sd_from_csv(fil, r=1, header=False):
    assert os.path.basename(fil).endswith('csv'), 'Please pass a csv file'
    with open(fil, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        if header:
            next(reader, None) # skip headers
        data = []
        for row in reader:
            data.append(int(row[r]))
        return standard_deviation(data)

def mad_from_csv(fil, r=1, header=False):
    assert os.path.basename(fil).endswith('csv'), 'Please pass a csv file'
    with open(fil, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        if header:
            next(reader, None) # skip headers
        data = []
        for row in reader:
            data.append(int(row[r]))
        return mean_absolute_deviation(data)

def iqr_from_csv(fil, r=1, header=False):
    assert os.path.basename(fil).endswith('csv'), 'Please pass a csv file'
    with open(fil, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        if header:
            next(reader, None) # skip headers
        data = []
        for row in reader:
            data.append(int(row[r]))
        return iqr(data)

def iqr_r_from_csv(fil, r=1, header=False):
    assert os.path.basename(fil).endswith('csv'), 'Please pass a csv file'
    with open(fil, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        if header:
            next(reader, None) # skip headers
        data = []
        for row in reader:
            data.append(int(row[r]))
        return iqr_reduced(data)



def dat_from_csv(fil, r=1, header=False):
    assert os.path.basename(fil).endswith('csv'), 'Please pass a csv file'
    with open(fil, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        if header:
            next(reader, None) # skip headers
        data = []
        for row in reader:
            print(row[r])
            data.append(float(row[r]))
        return data

def dat_row_from_csv(fil, header=False):
    assert os.path.basename(fil).endswith('csv'), 'Please pass a csv file'
    with open(fil, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        if header:
            next(reader, None) # skip headers
        data = []
        for row in reader:
            data.append(row)
        return data

def mode(x):
    data = Counter(x)
    return data.most_common(1)[0][0]

    

# print(mean_absolute_deviation([1, 4, 4]))
# print(variance([1, 4, 4]))
# print(standard_deviation([1, 4, 4]))
# print(median_absolute_deviation([1, 4, 1]))

# print(percentile([5, 6, 3, 2, 1], 0.5))

v1 = [1, 2, 3]
v2 = [4, 5, 6]

vector_sum = 0
for x, y in zip(v1, v2):
    vector_sum += x * y

print(vector_sum)

random.shuffle(v1)
random.shuffle(v2)

# print(v21)

vector_sum2 = 0
for x, y in zip(v1, v2):
    vector_sum2 += x * y

print(vector_sum2)

